/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdlibrary.controllers;

import com.thesoftwareguild.consoleio.ConsoleIO;
import com.thesoftwareguild.dao.DvdLibraryDao;
import com.thesoftwareguild.model.Dvd;
import com.thesoftwareguild.model.Note;
import dvdlibrary.data.DvdLibraryDaoImpl;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christian Choi
 */
public class DVDLibraryControllers {

    private DvdLibraryDao dao;

    public DVDLibraryControllers(DvdLibraryDao dao) {
        this.dao = dao;
    }
    private ConsoleIO io = new ConsoleIO();

    public void run() {
        boolean isRunning = true;
        while (isRunning) {
            printMainMenu();

            int menuSelection = io.promptForIntInRange("", 1, 5);
            switch (menuSelection) {
                case 1:
                    createDVD();
                    break;
                case 2:
                    removeDVD();
                    break;
                case 3:
                    listAllDVDs();
                    break;
                case 4:
                    searchDVDByTitle();
                    break;
                case 5:
                    isRunning = false;
                    break;
            }
        }

    }

    private void printMainMenu() {
        io.print("\nChoose an option:\n1- Add new DVD\n2- Remove DVD by title"
                + "\n3- List all DVDs\n4- Search for a DVD by title\n5- Exit");
    }

    private void createDVD() {
        String title = io.promptForString("\nPlease enter DVD title");
        String release = io.promptForString("Please enter release date (mm-dd-yyyy)");
        String rating = io.promptForString("Please enter movie rating");
        String director = io.promptForString("Please enter director name");
        String studio = io.promptForString("Please enter studio name");
        List<Note> comments = new ArrayList();
        Dvd dvd = new Dvd();
        dvd.setTitle(title);
        try {
            dvd.setReleaseDate(DvdLibraryDaoImpl.dateFormat.parse(release));
        } catch (ParseException ex) {
            Logger.getLogger(DVDLibraryControllers.class.getName()).log(Level.SEVERE, null, ex);
        }
        dvd.setMpaaRating(rating);
        dvd.setDirector(director);
        dvd.setStudio(studio);
        dvd.setNotes(comments);
        dao.create(dvd);
    }

    private void removeDVD() {
        String title = io.promptForString("\nPlease enter the title of the DVD you would like to remove");
        List<Dvd> dvdList = dao.list();
        if (dvdList.isEmpty()) {
            io.println("\nThere are no DVDs with the title " + title);
        } else {
            Dvd dvdToRemove = null;
            if (dvdList.size() == 1) {
                for (Dvd dvd : dvdList) {
                    if (dvd.getTitle().equalsIgnoreCase(title)) {
                        dvdToRemove = dvd;
                        break;
                    }
                }
                if (dvdToRemove != null) {
                    dao.delete(dvdToRemove.getId());
                }
            } else {
                io.println("Here is a list of DVDs with the title " + title + ":");
                for (int i = 0; i < dvdList.size(); i++) {

                    io.println(String.format("%d %s %s", i + 1,
                            dvdList.get(i).getTitle(),
                            dvdList.get(i).getReleaseDate()));
                }
                int option = io.promptForInt("\nPlease enter a number corresponding to the DVD you want to remove") - 1;
                dvdToRemove = dvdList.get(option);
                String check = io.promptForString("\nAre you sure you want to remove this DVD? (yes/no)");
                if (check.equals("yes")) {
                    dao.delete(dvdToRemove.getId());
                }
            }
        }
    }

    private void listAllDVDs() {
        List<Dvd> dvdList = dao.list();
        if (!dvdList.isEmpty()) {
            io.println("\nHere is the current list of DVDs by title and release date:");
            for (Dvd dvd : dvdList) {
                io.println(String.format("Title: %s | Release Date: %s", dvd.getTitle(), dvd.getReleaseDate()));
            }
        } else {
            io.println("\nThe current list of DVDs is empty!");
        }
    }

    private void searchDVDByTitle() {
        String title = io.promptForString("Please enter the title of the DVD: ");
        List<Dvd> dvdList = dao.searchByTitle(title);
        if (dvdList.isEmpty()) {
            io.println("\nThere are no DVDs with the title " + title);
        } else {
            Dvd dvdToDisplay;
            for (Dvd dvd : dvdList) {
                if (dvd.getTitle().equalsIgnoreCase(title)) {
                    dvdToDisplay = dvd;
                    break;
                }
            }

            io.println("\nHere is a list of DVDs with the title " + title);

            for (int i = 0; i < dvdList.size(); i++) {

                io.println(String.format("%d %s %s", i + 1,
                        dvdList.get(i).getTitle(),
                        dvdList.get(i).getReleaseDate()));
            }
            int option = io.promptForIntInRange("\nPlease enter a number corresponding to the DVD you want to display.", 1, dvdList.size()) - 1;
            dvdToDisplay = dvdList.get(option);
            displayDVDInfo(dvdToDisplay);
            String choice = io.promptForString("\nDo you want to edit this DVD? (yes/no)");
            if (choice.equals("yes")) {
                try {
                    editDVD(dvdToDisplay);
                } catch (ParseException ex) {
                    Logger.getLogger(DVDLibraryControllers.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void displayDVDInfo(Dvd dvd) {
        io.println("\nDisplaying DVD Information:");
        io.println("The DVD title is " + dvd.getTitle());
        io.println("The DVD release date is " + dvd.getReleaseDate());
        io.println("The DVD rating is " + dvd.getMpaaRating());
        io.println("The DVD director name is " + dvd.getDirector());
        io.println("The DVD studio name is " + dvd.getStudio());
        io.println("The DVD list of comments is " + dvd.getNotes());
    }

    private void editDVD(Dvd dvd) throws ParseException {
        boolean shouldRun = true;
        try {
            while (shouldRun) {
                printDVDMenu();
                int choice = io.promptForIntInRange("", 1, 7);
                switch (choice) {
                    case 1:
                        String title = io.promptForString("\nPlease enter the new title");
                        dvd.setTitle(title);
                        break;
                    case 2:
                        String date = io.promptForString("\nPlease enter the new release date (mm-dd-yyyy)");
                        dvd.setReleaseDate(DvdLibraryDaoImpl.dateFormat.parse(date));
                        break;
                    case 3:
                        String rating = io.promptForString("\nPlease enter the new rating");
                        dvd.setMpaaRating(rating);
                        break;
                    case 4:
                        String director = io.promptForString("\nPlease enter the new director's full name");
                        dvd.setDirector(director);
                        break;
                    case 5:
                        String studio = io.promptForString("\nPlease enter the new studio's name");
                        dvd.setStudio(studio);
                        break;
                    case 6:
                        editComments(dvd);
                        break;
                    case 7:
                        io.println("Going back to main menu.");
                        shouldRun = false;
                }

            }
        } catch (IOException e) {
            io.println("There was a problem writing to the disk.");
        }
    }

    private void printDVDMenu() {
        io.print("\nChoose an option to edit:\n1- Title\n2- Release Date"
                + "\n3- Rating\n4- Director Name\n5- Studio Name\n6- Comments"
                + "\n7- Back to main menu");
    }

    private void editComments(Dvd dvd) throws IOException {
        printCommentMenu();
        int choice = io.promptForIntInRange("Please select from the above choices", 1, 4);
        switch (choice) {
            case 1:
                String comment = io.promptForString("Please enter comment to add: ");
                Note note = new Note();
                note.setContent(comment);
                dvd.getNotes().add(note);
                break;
            case 2:
                io.println("Here is the current list of comments: ");
                for (int i = 0; i < dvd.getNotes().size(); i++) {
                    io.println(String.format("%d %s", i + 1, dvd.getNotes().get(i)));
                }
                int removeIdx = io.promptForInt("Please enter a number corresponding to the comment you want to remove: ") - 1;
                String check = io.promptForString("Are you sure you want to remove this comment? (yes/no)");
                if (check.equals("yes")) {
                    dvd.getNotes().remove(removeIdx);
                }
                break;
            case 3:
                io.println("Here is the current list of comments:");
                for (int i = 0; i < dvd.getNotes().size(); i++) {
                    io.println(String.format("%d %s", i + 1, dvd.getNotes().get(i)));
                }
                int editIdx = io.promptForInt("Please enter a number corresponding to the comment you want to edit:") - 1;
                String change = io.promptForString("What would you like to say?");
                dvd.getNotes().remove(editIdx);
                Note editedNote = new Note();
                editedNote.setContent(change);
                dvd.getNotes().add(editedNote);
                break;
            case 4:
                io.println("Going back to main menu.");
                break;
        }

    }

    private void printCommentMenu() {
        io.println("\nChoose an option to edit comments:\n1- Add\n2- Delete"
                + "\n3- Edit\n4- Back to DVD menu\n5- Back to main menu");
    }

}
