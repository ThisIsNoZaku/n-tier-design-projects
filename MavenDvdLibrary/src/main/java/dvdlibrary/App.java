/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdlibrary;

import com.thesoftwareguild.dao.DvdLibraryDao;
import dvdlibrary.controllers.DVDLibraryControllers;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Christian Choi
 */
public class App {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        new DVDLibraryControllers(context.getBean("libraryDao", DvdLibraryDao.class)).run();
    }
}
