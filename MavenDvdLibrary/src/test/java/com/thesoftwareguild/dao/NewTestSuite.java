/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author apprentice
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({com.thesoftwareguild.dao.DvdLibraryTestMark.class, com.thesoftwareguild.dao.DvdTestJeen.class, com.thesoftwareguild.dao.DvdTestsSena.class, com.thesoftwareguild.dao.DvdTestsDamien.class, com.thesoftwareguild.dao.DVDTestsChristian.class, com.thesoftwareguild.dao.DVDTestBrandon.class, com.thesoftwareguild.dao.YanDVDLibraryTest.class})
public class NewTestSuite {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
