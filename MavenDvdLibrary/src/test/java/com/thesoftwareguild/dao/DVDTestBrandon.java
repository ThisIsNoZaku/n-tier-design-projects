package com.thesoftwareguild.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.thesoftwareguild.dao.DvdLibraryDao;
import com.thesoftwareguild.model.Dvd;
import com.thesoftwareguild.model.Note;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DVDTestBrandon {

    DvdLibraryDao dvdDao;
    Date date;
    SimpleDateFormat sdf;
    public DVDTestBrandon() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
//        dao = new DvdLibraryDaoImp(){};
        dvdDao = ctx.getBean("dvdLibraryDao", DvdLibraryDao.class);
        //dao = new DvdLibraryLambdaDaoImp(){};

        sdf = new SimpleDateFormat("MMddyyyy");
        Map<Integer, Dvd> dvdMap = new HashMap<>();

//        date = new Date(01-05-2015);
        Dvd d1 = new Dvd();
        Dvd d2 = new Dvd();
        Dvd d3 = new Dvd();
        Note n1 = new Note();
        Note n2 = new Note();
        Note n3 = new Note();

        //=================================


        d1.setId(0);
        d1.setTitle("Inglorious Basterds");
        d1.setDirector("Quentin Tarentino");
        d1.setStudio("Studio 1");
        d1.setMpaaRating("R");

        try {
            Date date1 = sdf.parse("08212009");
            d1.setReleaseDate(date1);
        } catch (ParseException ex) {
            Logger.getLogger(DVDTestBrandon.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        List<Note> notes1 = new ArrayList();
        
            n1.setContent("Good for le lady");
            n2.setContent("Lots of things my eyes cant unsee");

            notes1.add(n1);
            notes1.add(n2);
            
        d1.setNotes(notes1);
        dvdDao.create(d1);
        
        ////////////////////////////////////////////////
        
        d2.setId(1);
        d2.setTitle("movie2");
        d2.setStudio("Place");
        d2.setDirector("2");

        d2.setMpaaRating("R");
        
        try {
            Date date2 = sdf.parse("01012013");
            d2.setReleaseDate(date2);
        } catch (ParseException ex) {
            Logger.getLogger(DVDTestBrandon.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        List<Note> notes2 = new ArrayList();
            n1.setContent("note1");
            n2.setContent("note2");
            n3.setContent("note3");
            
            notes2.add(n1);
            notes2.add(n2);
            notes2.add(n3);
        
        d2.setNotes(notes2);
        dvdDao.create(d2);
        
        ////////////////////////
        d3.setId(2);
        d3.setTitle("movie3");
        d3.setStudio("studio3");
        d3.setDirector("3");

        d3.setMpaaRating("NC17");
        
        try {
            Date date3 = sdf.parse("02012013");
            d3.setReleaseDate(date3);
        } catch (ParseException ex) {
            Logger.getLogger(DVDTestBrandon.class.getName()).log(Level.SEVERE, null, ex);
        }
        // if no notes for the movie
//        List<Note> notes3 = new ArrayList();
//            n1.setContent("note1");
//            n2.setContent("note2");
//            n3.setContent("note3");
//            
//            notes3.add(n1);
//            notes3.add(n2);
//            notes3.add(n3);
//        
//        d3.setNotes(notes3);
        dvdDao.create(d3);
        

    }

    @After
    public void tearDown() {
    }

    //expectedSize --> 1
    @Test
    public void getAllDvds() {
        List<Dvd> a = dvdDao.list();
        int expectedSize = 3;

        int actual = a.size();

        Assert.assertEquals(expectedSize, actual);
    }

    //Title ---> Inglorious Basterds
    @Test
    public void getDVDById() {
        int id = 0;
        Dvd d = dvdDao.get(id);

        String expected = "Inglorious Basterds";
        String actual = d.getTitle();

        Assert.assertEquals(expected, actual);
    }
    
    //8/21/2009 Inglorious Basterds; 1/1/2013 movie2; 2/1/2013 movie3
    @Test
    public void getDVDdate(){
        int id = 0;
        Dvd d = dvdDao.get(id);

        Date expected=null;
        try {
            expected = sdf.parse("08212009");
        } catch (ParseException ex) {
            Logger.getLogger(DVDTestBrandon.class.getName()).log(Level.SEVERE, null, ex);
        }

        Date actual = d.getReleaseDate();
//        System.out.println("Date " + actual);
        Assert.assertEquals(expected, actual);
        
    }

    //3 notes
    @Test
    public void getNumberOfNotes() {
        int input = 0;
        Dvd d = dvdDao.get(input);

        
        int expected = 2;
        
        int actual = d.getNotes().size();

        Assert.assertEquals(expected, actual);
    }  

    //size ---> >1
    @Test
    public void searchByTitle() {
        String input = "Inglorious Basterds";
        List<Dvd> d = dvdDao.searchByTitle(input);

        int expectedSize = 1;
        int actual = d.size();

        Assert.assertEquals(expectedSize, actual);
    }

    //size --> 0
    @Test
    public void searchByTitle2() {
        String input = "Little Miss Sunshine";
        List<Dvd> d = dvdDao.searchByTitle(input);

        int expectedSize = 0;
        int actual = d.size();

        Assert.assertEquals(expectedSize, actual);
    }

    @Test
    public void searchByDirector() {

        String input = "Quentin Tarentino";
        List<Dvd> d = dvdDao.searchByDirector(input);

        int expectedSize = 1;
        int actual = d.size();

        Assert.assertEquals(expectedSize, actual);

    }

    @Test
    public void searchByDirector2() {

        String input = "2";
        List<Dvd> d = dvdDao.searchByDirector(input);

        int expectedSize = 1;
        int actual = d.size();

        Assert.assertEquals(expectedSize, actual);

    }

    @Test
    public void searchByStudio() {

        String input = "Not a real studio";
        List<Dvd> d = dvdDao.searchByStudio(input);

        int expectedSize = 0;
        int actual = d.size();

        Assert.assertEquals(expectedSize, actual);

    }

    @Test
    public void searchByStudio2() {

        String input = "Studio 1";
        List<Dvd> d = dvdDao.searchByStudio(input);

        int expectedSize = 1;
        int actual = d.size();

        Assert.assertEquals(expectedSize, actual);

    }

    @Test
    public void searchByMpaa() {

        String input = "R";
        List<Dvd> d = dvdDao.searchByMpaaRating(input);

        int expectedSize = 2;
        int actual = d.size();

        Assert.assertEquals(expectedSize, actual);

    }

    
    //searchNewerThanYear
    //8/21/2009 Inglorious Basterds; 1/1/2013 movie2; 2/1/2013 movie3
    @Test
    public void searchNewerThan(){
        List<Dvd> list = dvdDao.searchNewerThanYear(2010);
        int expectedSize = 2;
        int actual = list.size();
        Assert.assertEquals(expectedSize, actual);   
    }
    
    //getAverageNumberOfNotes
    // 5 notes, 3 dvds
    @Test
    public void getAverageNumberOfNotes(){
        double expectedAvg = (double) 5/3;
//        System.out.println("expectedAvg_NumNotes: " + expectedAvg);
        double actual = dvdDao.getAverageNumberOfNotes();
        Assert.assertEquals(expectedAvg, actual,0);
    }
    
    // findNewestDvd
        // 8/21/2009 Inglorious Basterds; 1/1/2013 movie2; 2/1/2013 movie3
    @Test
    public void findNewestDvd(){
        Dvd d = dvdDao.findNewestDvd();
        String expectedTitle = "movie3";
        String actual = d.getTitle();
        
        Assert.assertEquals(expectedTitle, actual);
        
    }
    
    // findOldestDvd
    // 8/21/2009 Inglorious Basterds; 1/1/2013 movie2; 2/1/2013 movie3
    @Test
    public void findOldestDvd(){
        Dvd d = dvdDao.findOldestDvd();
        String expectedTitle = "Inglorious Basterds";
        String actual = d.getTitle();
        
        Assert.assertEquals(expectedTitle, actual);
        
    }
    
    
    
    //remove one dvd --> size of list of all dvds should be 0
    @Test
    public void removeDVD() {
        Integer input = 1;
        dvdDao.delete(input);

        List<Dvd> a = dvdDao.list();
        int expectedSize = 2;
        int actual = a.size();

        Assert.assertEquals(expectedSize, actual);
    }

    @Test
    public void LoadFiles() {
        dvdDao.delete(1);

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("dvdCollection.txt")));

            while (sc.hasNext()) {

                String line = sc.nextLine();

                String[] properties = line.split("::");

                Dvd d = new Dvd();
                //id::title::mpaarating: director::studio::(int)userRating::rDate::notes
                d.setId(Integer.parseInt(properties[0]));
                d.setTitle(properties[1]);
                d.setMpaaRating(properties[2]);
                d.setDirector(properties[3]);
                d.setStudio(properties[4]);
                d.setUserRating(Integer.parseInt(properties[5]));

                Date date;
                SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
                try {
                    date = sdf.parse(properties[6]);
                    d.setReleaseDate(date);
//                d.setReleaseDate(properties[6]);
                } catch (ParseException ex) {
                    Logger.getLogger(DvdLibraryDao.class.getName()).log(Level.SEVERE, null, ex);
                }

                //d.setUserNote(properties[7])
                // 2nd str split to look for notes
                // notes separated by <<
                if (properties.length == 8) {   // don't do anything if prop7 notes is missing in text
                    String strNote = properties[7];  // takes in notes string
                    String[] strNotesArr = strNote.split("<<");

                    if (strNotesArr.length != 0) {
                        List<Note> listNotes = new ArrayList<>();

                        for (int i = 0; i < strNotesArr.length; i++) {
                            Note newNote = new Note();
                            newNote.setContent(strNotesArr[i]);
                            listNotes.add(newNote);
                        }
                        d.setNotes(listNotes);
//                        System.out.println("Notes:");
//                        for (Note n : listNotes) {
//                            System.out.println(n.getContent());
//                        }
                    }
                }// no notes

//                dvdMapTest.put(d.getId(), d);  
                dvdDao.create(d);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DvdLibraryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /////////////  WRITING
    @Test

      public void writeDVDCollection() {
    
        try {
            PrintWriter writer = new PrintWriter(new FileWriter("dvdCollectionTest.txt"));

            List<Dvd> listDvd = dvdDao.list();

            for (Dvd d: listDvd){
//                System.out.println("id: " + d.getId()); //#
                // make release date formatted
                SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
                String strReleaseDate = sdf.format(d.getReleaseDate());

                // make notes in one string
                String noteStr = "";
                if (d.getNotes()!= null){
                    List<Note> noteList = d.getNotes();
                    
                    for (Note note : noteList) {
                        noteStr += note.getContent() + "<<";
                    }
                    noteStr = noteStr.substring(0, noteStr.length() - 2); // 2 less with << at the end
                
                                    //id::title::mpaarating: director::studio::(int)userRating::rDate::notes
                    writer.printf("%d::%s::%s::%s::%s::%d::%s::%s",
                            d.getId(),
                            d.getTitle(),
                            d.getMpaaRating(),
                            d.getDirector(),
                            d.getStudio(),
                            d.getUserRating(),
    //                        d.getReleaseDate(),
    //                        d.getUserNote()
                            strReleaseDate,
                            noteStr
                    );
                 
                } else {//no notes
                    //id::title::mpaarating: director::studio::(int)userRating::rDate::notes
                    writer.printf("%d::%s::%s::%s::%s::%d::%s",
                            d.getId(),
                            d.getTitle(),
                            d.getMpaaRating(),
                            d.getDirector(),
                            d.getStudio(),
                            d.getUserRating(),
    //                        d.getReleaseDate(),
    //                        d.getUserNote()
                            strReleaseDate
                    );
                }
                writer.println("");
            }

            writer.flush();
            writer.close();

        } catch (IOException ex) {
            Logger.getLogger(DvdLibraryDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
