/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addressbook.controllers;

import addressbook.data.AddressBookDaoImpl;
import com.thesoftwareguild.consoleio.ConsoleIO;
import com.thesoftwareguild.dao.AddressBookDao;
import com.thesoftwareguild.model.Address;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookController {

    private AddressBookDao dao;
    private ConsoleIO io = new ConsoleIO();

    public AddressBookController(AddressBookDao bean) {
        this.dao = dao;
    }

    public void run() {
        int userSelect = 0;
        main:
        while (userSelect != 6) {
            printMenu();
            userSelect = io.promptForIntInRange("", 1, 6);
            switch (userSelect) {
                case 1:
                    addAddress();
                    break;
                case 2:
                    removeAddress();
                    break;
                case 3:
                    listAllAddresses();
                    break;
                case 4:
                    viewAddress();
                    break;
                case 5:
                    viewAddressCount();
                    break;
                case 6:
                    break main;
            }
        }
    }

    public void printMenu() {
        io.println("Choose an option:");
        io.println("1- Add an address");
        io.println("2- Remove an address");
        io.println("3- List all addresses");
        io.println("4- View an address");
        io.println("5- View how many addresses are available");
        io.println("6- Exit");
    }

    public void addAddress() {
        String firstName = io.promptForString("Enter the first name.");
        String lastName = io.promptForString("Enter the last name.");
        String streetNumber = io.promptForString("Enter the street number.");
        String streetName = io.promptForString("Enter the street name.");
        String city = io.promptForString("Enter the city.");
        String state = io.promptForString("Enter the state");
        String zip = io.promptForString("Enter the zip");
        Address address = new Address();
        address.setFirstName(firstName);
        address.setLastName(lastName);
        address.setStreetNumber(streetNumber);
        address.setStreetName(streetName);
        address.setCity(city);
        address.setZip(zip);
        address.setState(state);
        dao.create(address);
    }

    public void removeAddress() {
        String name = io.promptForString("Enter name to get address for:");
        List<Address> addresses = dao.list().stream().filter(a -> a.getLastName().equals(name)).collect(Collectors.toList());
        for (int i = 0; i < addresses.size(); i++) {
            io.print((i + 1) + "- ");
            printAddress(addresses.get(i));
        }
        int userSelect = io.promptForIntInRange("Delete which one? (0 to return to the menu)", 0, addresses.size()) - 1;
        if (userSelect > 0) {
            dao.list().remove(addresses.get(userSelect));
        }
    }

    private void listAllAddresses() {
        List<Address> addresses = new ArrayList<>(dao.list());
        for (int i = 0; i < addresses.size(); i++) {
            io.print((i + 1) + "- ");
            printAddress(addresses.get(i));
        }
        io.println("");
        io.promptForString("Press enter to continue...");
    }

    private void viewAddress() {
        String name = io.promptForString("Enter a last name to get address for:");
        List<Address> addresses = dao.list().stream().filter(a -> a.getLastName().equals(name)).collect(Collectors.toList());
        int userSelect = 0;
        if (addresses.size() > 1) {
            for (int i = 0; i < addresses.size(); i++) {
                io.print((i + 1) + "- ");
                printAddress(addresses.get(i));
            }
            userSelect = io.promptForIntInRange("View which one?", 1, addresses.size()) - 1;
        }
        if (!addresses.isEmpty()) {
            Address display = addresses.get(userSelect);
            printAddress(display);
            io.println("");
            io.promptForString("Press enter to continue...");
        } else {
            io.println("No addresses for that name were found.");
        }
    }

    private void viewAddressCount() {
        io.println(String.format("The address book contains %d addresses.", dao.list().size()));
        io.println("");
        io.promptForString("Press enter to continue...");
    }

    private void printAddress(Address address) {
        io.println(String.format("%s %s %s %s, %s %s", address.getFirstName(), address.getLastName(), address.getStreetNumber(), address.getStreetName(), address.getCity(), address.getState(), address.getZip()));
    }
}
